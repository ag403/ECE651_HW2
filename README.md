# HW 2

## Compiling
To compile this, run:
`javac -d bin -sourcepath src src/main.java`

## Running
To run this code, use:
### Linux / OSX
`java -cp bin; main`
### Windows
`java -cp bin: main`

